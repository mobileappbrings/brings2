var bringsApp = bringsApp || angular.module('starter');
 
bringsApp.factory('apiFactory', function($q,$http){
    return {
        getData:function(model,url){
			var deferred=$q.defer();
			$http({url:url,data:model,method:'post',headers: {'Content-Type': 'application/json'}})
			.then(function onSuccess(resp) {
			   	deferred.resolve(resp);
			  },function onError(error) {
			  	deferred.reject('Error While fetching data'+error);
			  });
			return deferred.promise;
	   	},
		postData:function(model,url){
			var deferred=$q.defer();
			$http({url:url,data:model,method:'post',headers: {'Content-Type': 'application/json'}})
			.then(function onSuccess(resp) {
			   	deferred.resolve(resp);
			  },function onError(error) {
			  	deferred.reject('Error While fetching data'+error);
			  });
			return deferred.promise;
	   	}
        }  
});