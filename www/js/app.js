var pictureSource; // picture source
var destinationType; // sets the format of returned value
var timerSet;
var refreshIntervalId;
var latLongForDist1;
var latLongForDist2;
var latLongForDist3;
var latLongForDist4;
var refreshIntervalIdBring;

const orderPlacedByUser = "Successfully your order placed";
const orderCancelledByUser = "Successfully your order cancelled";

const orderPaymentInitiatedByUser = "Order payment initiated by user";
const orderPaymentDoneByUser = "Order payment done by user";
const orderPaymentCancelledByUser = "Order payment cancelled by user";

const orderAcceptedByServiceman = "Serviceman accepted order";
const orderCancelledByServiceman = "Serviceman cancelled order";

const servicemanStarted = "Serviceman started";
const servicemanDelivered = "Serviceman delivered";
const servicemanReturned = "Serviceman returned";
const servicemanInteractedUser = "Serviceman interacted user";


//Global Constants


var bringsApp = angular.module('starter', ['ionic', 'ngCordova', 'config', 'onezone-datepicker', 'ion-google-place', 'plgn.ionic-segment',
        'oc.lazyLoad'
    ])
	.service("tempService", function() {
        this.woid = "";
    })
    .run(function($ionicPlatform,  $rootScope, $location, tempService,$interval) {
        $ionicPlatform.ready(function() {
 
            $rootScope.$on('$stateChangeSuccess', function() {

            $interval.cancel(refreshIntervalId);
            $interval.cancel(refreshIntervalIdBring);
        }); 
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            window.plugins.OneSignal.setLogLevel({
                logLevel: 0,
                visualLevel: 0
            });
            window.plugins.OneSignal.getIds(function(ids) {

                localStorage.set('playerId', ids.userId);
            });


            //the callBack function called when we click on the notification received
            var notificationOpenedCallback = function(jsonData) {
                // var checkData = JSON.stringify(jsonData);

                alert(jsonData.message);
                var mss = jsonData.message;

                var userMessage;
                if (jsonData.message != null) {
                    userMessage = mss.split('-')[0];

                }
                if (userMessage.toLowerCase().trim() == orderAcceptedByServiceman.toLowerCase().trim()) {
                    var id = jsonData.message.split('-')[1];
                    $rootScope.getOrderDetailsFromPush(id);
                    alert("Please make a payment");
                    $rootScope.hideLoading();
                    $location.path("/app/trackdetails");
                } else if (userMessage.toLowerCase().trim() == orderPaymentDoneByUser.toLowerCase().trim()) {
                    var id = jsonData.message.split('-')[1];
                    tempService.woid = id;

                    $rootScope.hideLoading();

                    $location.path("/app/startstop");
                } else {
                    $location.path("/app/currorderlist");

                }

            };

             window.plugins.OneSignal.init("25886aee-e9eb-488b-9920-ade1321296c7", {
                    googleProjectNumber: "890149315538"
                },
                notificationOpenedCallback);
             window.plugins.OneSignal.setSubscription(true);
             window.plugins.OneSignal.enableNotificationsWhenActive(true);



        });
    });


// Global InAppBrowser reference
var iabRef = null;

//load start event
function iabLoadStart(event) {
    /*  if (event.url.match("https://payu.herokuapp.com/success")) {
        // iabRef.close();
     } */
}

function iabLoadStop(event) {
    console.log(event.url)
    if (event.url.match("https://payu.herokuapp.com/success")) {


        iabRef.executeScript({
            code: "document.body.innerHTML"
        }, function(values) {
            //incase values[0] contains result string
            var a = getValue(values[0], 'mihpayid');
            var b = getValue(values[0], 'status');
            var c = getValue(values[0], 'unmappedstatus');

            localStorage.setItem("payuresponse", values[0]);
            //history.back();
            window.location.reload();
        });

        // iabRef.close();
    } else if (event.url.match("https://payu.herokuapp.com/failure")) {


        iabRef.executeScript({
            code: "document.body.innerHTML"
        }, function(values) {

            localStorage.setItem("paymentfailed", "500");

            window.location.reload();
        });

        // iabRef.close();
    }
}

//get values from inner HTML page i.e success page or failure page values
function getValue(source, key) {
    var pattern = key + '=(\\w+)(&amp;)?';
    var expr = new RegExp(pattern);
    var result = source.match(expr);
    return result[1];
}


//load error event
function iabLoadError(event) {
    alert(event.type + ' - ' + event.message);
}
//close event
function iabClose(event) {
    iabRef.removeEventListener('loadstart', iabLoadStart);
    iabRef.removeEventListener('loadstop', iabLoadStop);
    iabRef.removeEventListener('loaderror', iabLoadError);
    iabRef.removeEventListener('exit', iabClose);
}
// device APIs are available
//
function onDeviceReadyTest() {
    iabRef = window.open('payuBiz.html', '_blank', 'location=no');
    iabRef.addEventListener('loadstart', iabLoadStart);
    iabRef.addEventListener('loadstop', iabLoadStop);
    iabRef.addEventListener('loaderror', iabLoadError);
    iabRef.addEventListener('exit', iabClose);

}