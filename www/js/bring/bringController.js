angular.module('starter')
    .service('tempModels', function() {
        this.pickupdata = "";
        this.delivereddata = "";

    })
    .controller('bringController', ["$scope", "$localStorage", "$http", "$location", "$timeout", "$ionicPopup", "latLongService", "BRING_API_URL", "$cordovaGeolocation", "$rootScope", "$ionicLoading", "tempModels", "apiFactory", function($scope, $localStorage, $http, $location, $timeout, $ionicPopup, latLongService, BRING_API_URL, $cordovaGeolocation, $rootScope, $ionicLoading, tempModels, apiFactory) {

        $scope.formObj = {};

        //validation logic 
        $scope.validationOfPickAddr = function(val) {
            $scope.makeDisable = false;
            $scope.validPAddress = false;
        }
        $scope.validationOfPickNm = function(val) {
            $scope.makeDisable = false;
            $scope.validPnm = false;
        }
        $scope.validationOfPickNum = function(val) {
            $scope.makeDisable = false;
            $scope.validPCon = false;
        }
        $scope.validationOfDelAdd = function(val) {
            $scope.makeDisable = false;
            $scope.validDAddress = false;
        }
        $scope.validationOfDelNm = function(val) {
            $scope.makeDisable = false;
            $scope.validDnm = false;
        }
        $scope.validationOfDelNum = function(val) {
            $scope.makeDisable = false;
            $scope.validDCon = false;
        }
        //validation logic

        $scope.autoFirst = false;
        $scope.autoSecond = false;


        latLongForDist1 = 17.448960;
        latLongForDist2 = 78.407184;
        latLongForDist3 = 17.454112;
        latLongForDist4 = 78.415807;

        var finalDistance;



        init();

        function init() {

            latLongForDist1 = latLongService.fromLat;
            latLongForDist2 = latLongService.fromLon;


            latLongForDist3 = latLongService.toLat;
            latLongForDist4 = latLongService.toLon;



            var origin1 = {
                lat: latLongForDist1,
                lng: latLongForDist2
            };
            var destinationA = {
                lat: latLongForDist3,
                lng: latLongForDist4
            };

            if (typeof latLongForDist1 == 'string') {
                latLongForDist1 = parseFloat(latLongForDist1);
            }
            if (typeof latLongForDist2 == 'string') {
                latLongForDist2 = parseFloat(latLongForDist2);
            }
            if (typeof latLongForDist3 == 'string') {
                latLongForDist3 = parseFloat(latLongForDist3);
            }
            if (typeof latLongForDist4 == 'string') {
                latLongForDist4 = parseFloat(latLongForDist4);
            }

            var origin1 = {
                lat: latLongForDist1,
                lng: latLongForDist2
            };
            var destinationA = {
                lat: latLongForDist3,
                lng: latLongForDist4
            };

            var service = new google.maps.DistanceMatrixService;
            service.getDistanceMatrix({
                origins: [origin1],
                destinations: [destinationA],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function(response, status) {
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    alert('Error was: ' + status);
                    return;
                }
                var originList = response.originAddresses;
                var destinationList = response.destinationAddresses;

                var output2 = document.getElementById("myP");

                if (output2 != null) {
                    output2.innerHTML = "";

                    for (var i = 0; i < originList.length; i++) {
                        var results = response.rows[i].elements;
                        for (var j = 0; j < results.length; j++) {


                            output2.innerHTML += 'Total Distance ' + results[j].distance.text + ',without traffic may reach in ' + results[j].duration.text;
                            finalDistance = results[j].distance.text;
                            $rootScope.searchedFirst = false;
                            $rootScope.searchedSecond = false;
                            console.log(finalDistance)

                        }
                    }
                    $timeout(function() {
                        output2.innerHTML = output2.innerHTML;
                    }, 500);
                }
            });
        }



        $scope.descriptionValue = [{
            "id": 1,
            "name": "Commercial"
        }, {
            "id": 2,
            "name": "Non Commercial"
        }];

        //7.2-5=2.2*10=22


        $scope.getPriceNew = function(obj) {

            if (obj == undefined) {
                alert("Please enter weight");
                return;
            }
            $scope.makeDisable = false;
            selectedWeight = obj;
            if (finalDistance == null || finalDistance == undefined || finalDistance == '') {
                alert("Please click on check availability");
                return;
            }
            var dist = finalDistance.split(' ');
            var finalRate;
            var averageRatePerKg = 5;

            var finalDist123 = Math.round(dist[0] * 100) / 100;
            console.log(finalDist123)

            if (finalDist123 <= 5) {
                if (selectedWeight <= 5) {
                    $scope.dynamicValuePrice = 60;
                } else {
                    selectedWeight = selectedWeight - 5;
                    $scope.dynamicValuePrice = 60 + (selectedWeight * 5);
                }
            } else {
                if (selectedWeight <= 5) {
                    finalDist123 = (finalDist123 - 5);
                    $scope.dynamicValuePrice = 60 + (finalDist123 * 5);
                } else {
                    finalDist123 = finalDist123 - 5;
                    selectedWeight = selectedWeight - 5;
                    $scope.dynamicValuePrice = 60 + (finalDist123 * 5) + (selectedWeight * 5);
                }
            }

        }

        function m1() {
            var weight;
            var dist;
            var price;
            if (dist <= 5) {
                if (weight <= 5) {
                    price = 60;
                } else {
                    weight = weight - 5;
                    price = 60 + (weight * 5);
                }
            } else {
                if (weight <= 5) {
                    dist = (dist - 5);
                    price = 60 + (dist * 5);
                } else {
                    dist = dist - 5;
                    weight = weight - 5;
                    price = 60 + (dist * 5) + (weight * 5);
                }
            }
        }

        $scope.gPlace;
        var fromGooglePlaceAddress;
        $scope.newFromPlace = function(chosenPlace) {
            console.log(chosenPlace)
        }

        $scope.newToPlace = function(chosenPlace) {
            console.log(chosenPlace)
        }

        $scope.checkAvailBring = function() {
            var checkEmail = $localStorage.get('email');
            if (checkEmail == undefined) {
                var popUp = $ionicPopup.alert({
                    title: 'Message',
                    template: 'Please login to use this service',
                    cssClass: 'popcss'
                });
                //return;
            }
            if (latLongService.fromLat == "" || latLongService.toLat == "") {
                var popUp = $ionicPopup.alert({
                    title: 'Message',
                    template: 'Please enter location',
                    cssClass: 'popcss'
                });
                return;
            }



            init();

            var obj = {
                "fromLat": "" + latLongService.fromLat + "",
                "fromLng": "" + latLongService.fromLon + "",
                "toLat": "" + latLongService.toLat + "",
                "toLng": "" + latLongService.toLon + ""
            }
            var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/checkFirstAvailability";
            apiFactory.postData(obj, url).then(function(success) {
                if (success.response == "412") {
                    var popUp = $ionicPopup.alert({
                        template: 'Server issue, please try again',
                        cssClass: 'popcss'
                    })
                }
                if (success.response == "308") {
                    var popUp = $ionicPopup.alert({
                        template: 'Serviceman not availiable',
                        cssClass: 'popcss'
                    })
                }
                if (success.response == "207") {
                    var popUp = $ionicPopup.alert({
                        template: 'Serviceman will be availiable,Please proceed',
                        cssClass: 'popcss'
                    })
                    $scope.serviceAvail = true;

                    $scope.formObj.PickCompleteAddress = tempModels.pickupdata;
                    $scope.formObj.deliverCompleteAddress = tempModels.delivereddata;
                    console.log(tempModels.pickupdata)
                    console.log(tempModels.delivereddata)

                }
            });


        }

        $scope.doBring = function(bringForm) {
            $scope.formObj = bringForm;

            //validation logic
            $scope.makeDisable = true;

            if (bringForm.PickCompleteAddress == undefined) {
                $scope.validPAddress = true;
            } else {
                $scope.makeDisable = false;
            }

            if (bringForm.pickContactPerson == undefined) {
                $scope.validPnm = true;
            } else {
                $scope.makeDisable = false;
            }
            if (bringForm.pickContactNumber == undefined) {
                $scope.validPCon = true;
            } else {
                $scope.makeDisable = false;
            }


            if (bringForm.deliverCompleteAddress == undefined) {
                $scope.validDAddress = true;
            } else {
                $scope.makeDisable = false;
            }
            if (bringForm.deliContactPerson == undefined) {
                $scope.validDnm = true;
            } else {
                $scope.makeDisable = false;
            }
            if (bringForm.delveryContactNumber == undefined) {
                $scope.validDCon = true;
            } else {
                $scope.makeDisable = false;
            }
            if (bringForm.estWeight1 == undefined || bringForm.estWeight1 == '') {
                alert("Estimated weight mandatory");
                $scope.makeDisable = true;
                return;
            }
            if (bringForm.delveryContactNumber == undefined || bringForm.deliContactPerson == undefined ||
                bringForm.deliverCompleteAddress == undefined || bringForm.pickContactNumber == undefined ||
                bringForm.pickContactPerson == undefined || bringForm.PickCompleteAddress == undefined
            ) {
                $scope.makeDisable = true;
            }
            //validation logic
            var doBringObj = {
                "buyerid": $localStorage.get('email'),
                "type": "BringGoods",
                "fromaddress": latLongService.fromLoc,
                "fromlat": "" + latLongService.fromLat + "",
                "fromlng": "" + latLongService.fromLon + "",
                "toaddress": latLongService.toLoc,
                "tolat": "" + latLongService.toLat,
                "tolng": "" + latLongService.toLon + "",
                "packdgoodspersonname": bringForm.pickContactPerson,
                "packdgoodscontactno": bringForm.pickContactNumber,
                "description": bringForm.prodDesc.name,
                "nameDelPerson": bringForm.deliContactPerson,
                "contDelPerson": bringForm.delveryContactNumber,
                "estimatedCost": $scope.dynamicValuePrice,
                "estimatedWeight": bringForm.estWeight1,
                "deladdressop": bringForm.deliverCompleteAddress,
                "pickaddressop": bringForm.PickCompleteAddress,
                "extradesc": bringForm.EXTRA_DESC
            }

            $rootScope.finalBringObject = doBringObj;
            $location.path('/app/summary');



        }
    }]).controller('ordersummary', ["$scope", "$localStorage", "$http", "$location", "$timeout", "$ionicPopup", "latLongService", "BRING_API_URL", "$cordovaGeolocation", "$rootScope", "$ionicLoading", "$ionicHistory", "$interval", "userService", "apiFactory", function($scope, $localStorage, $http, $location, $timeout, $ionicPopup, latLongService, BRING_API_URL, $cordovaGeolocation, $rootScope, $ionicLoading, $ionicHistory, $interval, userService, apiFactory) {

        $rootScope.stopLoading = function() {
            $rootScope.hideLoading();
            alert("Cancelling your order, please wait");

            var obj = {
                "WORK_ORDER_NUMBER": wodOfUser,
                "Email": $localStorage.get('email')
            }
            var url = BRING_API_URL.apiEndpoint + "rest/cancel/bybuyer";
            apiFactory.postData(obj, url).then(function(succ) {
                if (succ.response == '-1') {
                    alert("Payment done so, order cannot be cancelled.");
                }
                if (succ.response == '1') {
                    alert("Order cancelled");
                }
                if (succ.response == '-2') {
                    alert("Server issue")
                }
            });
            $location.path('/app/dashboard');
            location.reload();

        }
        var wodOfUser;
        $scope.finalBring = function() {
            var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/bringGoods";
            apiFactory.postData($rootScope.finalBringObject, url).then(function(succ) {
                if (succ.response == '-1') {
                    alert("Payment done so, order cannot be cancelled.");
                }
                if (succ.response == '1') {
                    alert("Order cancelled");
                }
                if (succ.response == '-2') {
                    alert("Server issue")
                }
            });
            var intrvl;
            intrvl = $interval(function() {
                $rootScope.checkServicemenFlag = $localStorage.get('userMode');

                if ($rootScope.checkServicemenFlag != "true") {

                    var sendObj = {
                        "userId": $localStorage.get('email'),
                        "WOD": wodOfUser,
                        'role': $localStorage.get('userType')
                    }
                    var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/getOrderById";
                    apiFactory.postData(sendObj, url).then(function(succ) {
                        if (succ.length > 0) {

                            if (succ[0].SERVICE_PERSON_CONFIRMATION == 'YES') {
                                alert("Serviceman confirmed your order, please pay online for deliver");
                                $rootScope.hideLoading();

                                $localStorage.set("WORK_ORDER_NUMBER", sendObj.WOD);


                                $rootScope.detailsObj = succ[0];

                                userService.userObj = succ[0];

                                if ($rootScope.detailsObj.ORDER_TYPE != 'BringGoods') {
                                    var ITEMS = $rootScope.detailsObj.ITEMS;
                                    var ArrArr = [];
                                    var ArrArr2 = [];
                                    ITEMS = ITEMS.split("$");
                                    for (var i = 0; i < ITEMS.length; i++) {
                                        var objs = ITEMS[i].split("#");
                                        console.log(objs);
                                        ArrArr.push(objs);
                                    }
                                    console.log(ArrArr);

                                    $rootScope.detailsObj.ITEMS = ArrArr;
                                    console.log($rootScope.detailsObj)
                                }
                                $location.path("/app/trackdetails");
                                $ionicHistory.clearCache();
                                $ionicHistory.clearHistory();
                                $interval.cancel(intrvl);
                            }
                        }
                    });
                }
            }, 15000);


        }

        $scope.cancel = function() {
            $location.path('/app/bring');
        }

    }])