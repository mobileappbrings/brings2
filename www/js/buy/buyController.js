angular.module('starter')
.controller('buyController', ["$scope", "$localStorage", "$http", "$location", "$timeout", "$ionicPopup", "BRING_API_URL", "latLongService", "apiFactory", function($scope, $localStorage, $http, $location, $timeout, $ionicPopup, BRING_API_URL, latLongService, apiFactory) {
    $scope.gPlace;
    var googlePlaceAddress;
    $scope.makeDisable = true;

    $scope.checkAvailBuy = function() {

        if (latLongService.toLat == "") {
            var popUp = $ionicPopup.alert({
                title: 'Message',
                template: 'Please enter address',
                cssClass: 'popcss'
            });
            return;
        }
        var obj = {
            "toLat": "" + latLongService.toLat + "",
            "toLng": "" + latLongService.toLon + ""
        }
        var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/checkFirstBuyerAvailability";
        apiFactory.postData(obj, url).then(function(success) {
            if (success.response == "413") {
                var popUp = $ionicPopup.alert({
                    template: 'Server issue, please try again',
                    cssClass: 'popcss'
                })
            }
            if (success.response == "309") {
                var popUp = $ionicPopup.alert({
                    template: 'Serviceman not availiable',
                    cssClass: 'popcss'
                })
            }
            if (success.response == "208") {
                var popUp = $ionicPopup.alert({
                    template: 'Serviceman will be availiable,Please proceed',
                    cssClass: 'popcss'
                })
                $scope.serviceAvail = true;
            }
        });
    }
    $scope.doBuy = function(buyForm) {

        if (buyForm.toAddress == undefined) {
            $scope.validPAddress = true;
        } else {
            $scope.makeDisable = false;
        }

        if (buyForm.ContactPerson == undefined) {
            $scope.validDnm = true;
        } else {
            $scope.makeDisable = false;
        }
        if (buyForm.ContactNumber == undefined) {
            $scope.validPCon = true;
        } else {
            $scope.makeDisable = false;
        }


        if (buyForm.FullAddress == undefined) {
            $scope.validDCon = true;
        } else {
            $scope.makeDisable = false;
        }

        if (buyForm.toAddress == undefined || buyForm.ContactPerson == undefined ||
            buyForm.ContactNumber == undefined || buyForm.ContactNumber == undefined || buyForm.FullAddress == undefined

        ) {
            $scope.makeDisable = true;
            return;
        }

        var itemListObj = {};
        for (var i = 0; i < $scope.choices.length; i++) {
            var j = i + 1;
            var mykey = "ch" + j;
            itemListObj[mykey] = $scope.choices[i].item;
        }
        var itemsOfList = JSON.stringify(itemListObj);

        if (itemListObj.ch1 == '') {
            $scope.validItem = true;
            $scope.makeDisable = true;
            return;
        } else {
            $scope.validItem = false;
            $scope.makeDisable = false;
        }
        var checkLogin = $localStorage.get('email');
        if (checkLogin == undefined) {
            alert("Please login to use the service");
            return;
        }
        var doBuyObj = {
            "buyerid": $localStorage.get('email'),
            "type": "BuyGoods",
            "toaddress": buyForm.toAddress,
            "tolat": "" + latLongService.toLat + "",
            "tolng": "" + latLongService.toLon + "",
            "personname": buyForm.ContactPerson,
            "contactno": buyForm.ContactNumber,
            "description": buyForm.FullAddress,
            "listofitems": itemsOfList,
            "deladdressop": buyForm.FullAddress
        }
        console.log(doBuyObj);
        var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/buyGoods";
        apiFactory.postData(doBuyObj, url).then(function(success) {
            var popUp = $ionicPopup.alert({
                title: 'Success',
                template: 'Successfully Booked Order',
                cssClass: 'popcss'
            });

            $timeout(function() {
                popUp.close();
                $location.path("#/app/dashboard");
            }, 2000);
        });
    }

    $scope.choices = [{
        item: ''
    }];
    $scope.addNewChoice = function() {
        console.log("aaaaaaa")
        $scope.choices.push({
            'item': ''
        });
    };
    $scope.removeChoice = function(removeItem) {
        console.log("eeeeeeeee")
        $scope.choices.splice(removeItem, 1);
    };

    //validation logic 
    $scope.validationOfPickAddr = function(val) {
        $scope.makeDisable = false;
        $scope.validPAddress = false;
    }
    $scope.validationOfPickContNum = function(val) {
        $scope.makeDisable = false;
        $scope.validPCon = false;
    }

    $scope.validationOfPickContName = function(val) {
        $scope.makeDisable = false;
        $scope.validDnm = false;
    }

    $scope.validationOfPickFulladd = function(val) {
        $scope.makeDisable = false;
        $scope.validDCon = false;
    }
    $scope.validationOfItem = function(val) {
        $scope.validItem = false;
        $scope.makeDisable = false;
    }

}])