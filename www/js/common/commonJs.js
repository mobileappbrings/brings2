angular.module('starter')
    .directive('ionBottomSheet', [function() {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            controller: [function() {}],
            template: '<div class="modal-wrapper" ng-transclude></div>'
        };
    }])
    .directive('ionBottomSheetView', function() {
        return {
            restrict: 'E',
            compile: function(element) {
                element.addClass('bottom-sheet modal');
            }
        };
    })
    .service('LocationService', function($q) {

        var autocompleteService = new google.maps.places.AutocompleteService();

        var detailsService = new google.maps.places.PlacesService(document.createElement("input"));
        return {
            searchAddress: function(input) {
                var deferred = $q.defer();

                autocompleteService.getPlacePredictions({
                    input: input
                }, function(result, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        console.log(status);
                        deferred.resolve(result);
                    } else {
                        deferred.reject(status)
                    }
                });

                return deferred.promise;
            },
            getDetails: function(placeId) {
                var deferred = $q.defer();
                detailsService.getDetails({
                    placeId: placeId
                }, function(result) {
                    deferred.resolve(result);
                });
                return deferred.promise;
            }
        };
    })
    .directive('locationSuggestion', function($ionicModal, LocationService, latLongService, $location, $cordovaGeolocation, $rootScope, $ionicPopup, $localStorage, $http, $timeout, tempModels) {
        return {
            restrict: 'A',
            scope: {
                location: '='
            },
            link: function($scope, element) {

                var email = $localStorage.get("email");
                $scope.addressArray = [];
                if (email != undefined) {
                    var dataObj = {
                        "email": email
                    }
                    $http.post("http://brings.co.in/Cluster/rest/SaveAddress/getSavedAdress", dataObj).success(function(data) {
                        console.log(data);
                        $scope.addressArray.push(data.response);


                    }).error(function(err) {

                    });
                }


                $scope.checkLocation = function(val) {

                    var address = val;

                    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false';
                    $.get(url, function(data) {
                        if (data.status == 'OK') {

                            console.log(data.results[0].geometry.location);

                            $scope.savedLatForAdd = data.results[0].geometry.location.lat;
                            $scope.savedLonForAdd = data.results[0].geometry.location.lng;

                            console.log($scope.savedLatForAdd);
                            console.log($scope.savedLonForAdd);
                        }
                    });
                }




                $scope.selectdAddressFun = function(obj) {
                    console.log(obj)
                    $scope.location = {};
                    $scope.formObj = {};
                    $scope.location.formatted_address = obj.ADDRESS;
                    latLongService.fromLat = obj.LAT;
                    latLongService.fromLon = obj.LNG;
                    tempModels.pickupdata = obj.flat + "," + obj.street + "," + obj.street;
                    latLongForDist1 = latLongService.fromLat;
                    latLongForDist2 = latLongService.fromLon;
                    latLongService.fromLoc = obj.ADDRESS;


                    $scope.close();
                }

                var posOptions = {
                    enableHighAccuracy: false,
                    timeout: 30000,
                    maximumAge: 0
                };
                $scope.getLocationCurr = function(data) {

                    if (data == "a") {

                        $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position) {
                            var lat = position.coords.latitude;
                            var lon = position.coords.longitude;

                            latLongService.lat1 = lat;
                            latLongService.lon1 = lon;

                            latLongService.fromLat = lat;
                            latLongService.fromLon = lon;

                            $rootScope.searchedFirst = true;

                            var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + '%2C' + position.coords.longitude + '&language=en';

                            $.getJSON(GEOCODING).done(function(location) {

                                $scope.location = {};
                                $scope.location.formatted_address = location.results[0].formatted_address;
                                latLongService.fromLoc = location.results[1].formatted_address;

                                $scope.modal.hide();
                            })


                        });

                    }


                }
                if ($location.path() != '/app/trackdetails') {

                    clearInterval(timerSet);
                }
                $scope.search = {};
                $scope.search.suggestions = [];
                $scope.search.query = "";
                $ionicModal.fromTemplateUrl('location.html', {
                    scope: $scope,
                    animation: 'slide-in-up',
                    focusFirstInput: true
                }).then(function(modal) {
                    $scope.modal = modal;
                });

                element[0].addEventListener('focus', function(event) {
                    $scope.open();
                });
                $scope.$watch('search.query', function(newValue) {
                    if (newValue) {
                        LocationService.searchAddress(newValue).then(function(result) {
                            $rootScope.searchedFirst = false;

                            $scope.search.error = null;
                            $scope.search.suggestions = result;
                        }, function(status) {
                            $scope.search.error = "There was an error :( " + status;
                        });
                    };
                    $scope.open = function() {
                        $scope.modal.show();
                    };
                    $scope.close = function() {
                        $scope.modal.hide();
                    };
                    $scope.choosePlace = function(place) {
                        LocationService.getDetails(place.place_id).then(function(location) {
                            $scope.location = location;
                            var addressComponents = [];
                            var geoComponents = $scope.location;
                            latLongService.fromLat = geoComponents.geometry.location.lat();

                            latLongService.fromLon = geoComponents.geometry.location.lng();
                            latLongService.fromLoc = $scope.location.formatted_address;
                            $scope.close();
                        });
                    };

                });
            }
        }
    })
    .directive('locationSuggestions', function($ionicModal, LocationService, latLongService, $location, $cordovaGeolocation, $rootScope, $ionicPopup, $localStorage, $http, tempModels) {
        return {
            restrict: 'A',
            scope: {
                location: '='
            },
            link: function($scope, element) {

                var email = $localStorage.get("email");
                $scope.addressArray = [];
                if (email != undefined) {
                    var dataObj = {
                        "email": email
                    }
                    $http.post("http://brings.co.in/Cluster/rest/SaveAddress/getSavedAdress", dataObj).success(function(data) {
                        console.log(data);
                        $scope.addressArray.push(data.response);
                        console.log($scope.addressArray);

                    }).error(function(err) {

                    });
                }




                $scope.checkLocation = function(val) {

                    var address = val;

                    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false';
                    $.get(url, function(data) {
                        if (data.status == 'OK') {

                            console.log(data.results[0].geometry.location);

                            $scope.savedLatForAdd = data.results[0].geometry.location.lat;
                            $scope.savedLonForAdd = data.results[0].geometry.location.lng;


                        }
                    });
                }




                $scope.selectdAddressFun = function(obj) {
                    tempModels.delivereddata = obj.flat + "," + obj.street + "," + obj.street;
                    console.log(obj);
                    $scope.location1 = {};
                    $scope.location1.formatted_address = obj.ADDRESS;
                    console.log($scope.location1.formatted_address);
                    document.getElementById('location1').value = $scope.location1.formatted_address;
                    latLongService.toLat = obj.LAT;
                    latLongService.toLon = obj.LNG;
                    console.log(latLongService.toLat);

                    latLongForDist3 = latLongService.toLat;
                    latLongForDist4 = latLongService.toLon;
                    latLongService.toLoc = obj.ADDRESS;
                    $scope.close();

                }

                var posOptions = {
                    enableHighAccuracy: false,
                    timeout: 30000,
                    maximumAge: 0
                };
                $scope.getLocationCurrLocation1 = function() {
                    var posOptions = {
                        enableHighAccuracy: false,
                        timeout: 30000,
                        maximumAge: 0
                    };

                    $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position) {
                        var lat = position.coords.latitude;
                        var lon = position.coords.longitude;

                        latLongService.lat2 = lat;
                        latLongService.lon2 = lon;

                        latLongService.toLat = lat;
                        latLongService.toLon = lon;

                        $rootScope.searchedSecond = true;

                        var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + '%2C' + position.coords.longitude + '&language=en';

                        $.getJSON(GEOCODING).done(function(locations) {
                            $scope.location1 = {};
                            $scope.location1.formatted_address = locations.results[0].formatted_address;
                            latLongService.toLoc = locations.results[1].formatted_address;
                            document.getElementById('location1').value = $scope.location1.formatted_address;
                            $scope.modal.hide();
                        })


                    });



                }


                $scope.search = {};
                $scope.search.suggestions = [];
                $scope.search.query = "";

                $ionicModal.fromTemplateUrl('locationTo.html', {
                    scope: $scope,
                    focusFirstInput: true,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.modal = modal;
                });

                element[0].addEventListener('focus', function(event) {
                    $scope.open();
                });
                $scope.$watch('search.query', function(newValue) {
                    if (newValue) {
                        LocationService.searchAddress(newValue).then(function(result) {
                            $rootScope.searchedSecond = false;
                            $scope.search.error = null;
                            $scope.search.suggestions = result;
                        }, function(status) {
                            $scope.search.error = "There was an error :( " + status;
                        });
                    };
                    $scope.open = function() {
                        $scope.modal.show();
                    };
                    $scope.close = function() {
                        $scope.modal.hide();
                    };
                    $scope.chooseToPlace = function(place) {
                        LocationService.getDetails(place.place_id).then(function(location) {
                            $scope.location = location;
                            var addressComponents = [];
                            var geoComponents = $scope.location;

                            latLongService.toLat = geoComponents.geometry.location.lat();
                            latLongService.toLon = geoComponents.geometry.location.lng();
                            latLongService.toLoc = $scope.location.formatted_address;

                            $scope.close();
                        });
                    };
                });
            }
        }
    })
    .service('latLongService', function() {
        this.fromLat = "";
        this.fromLon = "";
        this.fromLoc = "";

        this.toLat = "";
        this.toLon = "";
        this.toLoc = "";

        this.loginloLat = "";
        this.logintoLon = "";
        this.logintoLoc = "";

        this.setUserFlag = "";

        this.lat1 = "";
        this.lon1 = "";

        this.lat2 = "";
        this.lon2 = "";



    }).directive('ngFiles', ['$parse', function($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function(event) {
                onChange(scope, {
                    $files: event.target.files
                });
            });
        };

        return {
            link: fn_link
        }
    }])
    .directive('googleplace', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, model) {
                var options = {
                    types: [],
                    componentRestrictions: {
                        country: 'in'
                    }
                };
                scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

                google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                    var addressComponents = [];
                    var geoComponents = scope.gPlace.getPlace();
                    var latitude = geoComponents.geometry.location.lat();
                    var longitude = geoComponents.geometry.location.lng();
                    addressComponents.push(element.val(), latitude, longitude);
                    scope.$apply(function() {
                        scope.details = addressComponents;
                        model.$setViewValue(addressComponents);
                    });
                });
            }
        };
    })
    .directive('googleplace1', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, model) {
                var options = {
                    types: [],
                    componentRestrictions: {
                        country: 'in'
                    }
                };
                scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

                google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                    var addressComponents = [];
                    var geoComponents = scope.gPlace.getPlace();
                    var latitude = geoComponents.geometry.location.lat();
                    var longitude = geoComponents.geometry.location.lng();
                    addressComponents.push(element.val(), latitude, longitude);
                    scope.$apply(function() {
                        scope.details = addressComponents;
                        model.$setViewValue(addressComponents);
                    });
                });
            }
        };


    })
    .directive('file', function() {
        return {
            restrict: 'AE',
            scope: {
                file: '@'
            },
            link: function(scope, el, attrs) {
                el.bind('change', function(event) {
                    var files = event.target.files;
                    var file = files[0];
                    if (file && typeof(file) !== undefined && file.size > 0) {
                        scope.file = file;
                        scope.$parent.file = file;
                    } else {
                        scope.file = {};
                        scope.$parent.file = {};
                    }
                    scope.$apply();
                });
            }
        };
    })
    .directive('noSpecialChar', function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function(inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('noChars', function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function(inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^0-9]/, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('noNumbers', function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function(inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[0-9]/g, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('ionRangeSlider', function ionRangeSlider() {
        return {
            restrict: 'A',
            scope: {
                rangeOptions: '=',
                model: '=ngModel',
                apply: '=apply'
            },
            link: function(scope, elem, attrs) {
                elem.ionRangeSlider(scope.rangeOptions);
                scope.$watch('apply', function() {
                    if (scope.apply) {
                        scope.apply = false;
                        var slider = elem.data("ionRangeSlider");
                        slider.update({
                            from: scope.model
                        });
                    }
                });
            }
        }
    })
    .service('userService', function($cordovaGeolocation, $localStorage) {
        this.userObj = "";
        this.userWorkOrderNo = "";

    })