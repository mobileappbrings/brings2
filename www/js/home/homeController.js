angular.module('starter')
    .controller('PlaylistsCtrl', function($scope, $state, $ionicConfig, $http, BRING_API_URL) {
        $ionicConfig.backButton.text("")


    })
    .controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicNavBarDelegate, $localStorage, $rootScope, $ionicHistory, $window, latLongService, $http, $ionicHistory, BRING_API_URL, $cordovaGeolocation, $location, $interval, $cordovaSocialSharing, $ionicLoading, $ionicPlatform,apiFactory) {


        $rootScope.showLoading = function() {
            $ionicLoading.show({
                templateUrl: 'loading.html'
            });
        }
        $rootScope.hideLoading = function() {
            $ionicLoading.hide();
        }
        $scope.testone123 = false;

        $scope.shareAnywhere = function() {

            if ($localStorage.get('email') != undefined) {
                var obj = {
                    "Email": $localStorage.get('email')
                }
                var referralCode;
                $http.post("http://brings.co.in/Cluster/rest/wallet/user", obj).success(function(succ) {
                    $scope.referralCode = succ.referral_id;
                    referralCode = angular.copy($scope.referralCode);

                }).error(function(err) {
                    alert(err)
                });

                var options = {
                    message: 'Brings App', // not supported on some apps (Facebook, Instagram)
                    subject: 'download brings app on playstore use referral code ' + referralCode,
                    url: 'https://play.google.com/store/apps/details?id=brings.co.in&hl=en',
                    chooserTitle: 'Pick an app'
                }
                window.plugins.socialsharing.shareWithOptions(options, function() {}, function() {});

            }

        }
        $scope.getUserCredit = function() {
            if ($localStorage.get('email') != undefined) {
                var obj = {
                    "Email": $localStorage.get('email')
                };
                var url = BRING_API_URL.apiEndpoint + "wallet/user";
                apiFactory.postData(obj, url).then(function(success) {
                    $scope.referralCode = succ.referral_id;
                });


            }
        }
        $scope.getUserCredit();

        $scope.checkServicemen = false;
        $rootScope.checkServicemen2 = false;
        $scope.isLoggedIn = function() {
            $scope.checkServicemen = $localStorage.get('cred');
            $rootScope.checkServicemen2 = $localStorage.get('cred');
            $scope.loggedIn = $localStorage.get('rootGlobals');
        }

        if ($localStorage.get('rootGlobals') != undefined) {
            $scope.loggedIn = $localStorage.get('rootGlobals');
        }

        $scope.isLoggedIn();
        $scope.sendLatLongFromServiceman = function() {

            $scope.lat = null;
            $scope.longt = null
            var posOptions = {
                timeout: 10000,
                enableHighAccuracy: false
            };

            $cordovaGeolocation
                .getCurrentPosition(posOptions)

                .then(function(position) {
                    $scope.lat = position.coords.latitude;
                    $scope.longt = position.coords.longitude;

                    var obj = {
                        "lat": "" + $scope.lat + "",
                        "lon": "" + $scope.longt + "",
                        "userid": $localStorage.get("email")
                    }
                    var url = BRING_API_URL.apiEndpoint + "Map/updateLocation";
                    apiFactory.postData(obj, url).then(function(success) {
                        if (succ.response == 288) {
                            alert("Please submit your proofs to start earning");
                            $location.path('/app/MyProfile');
                        }
                    });

                }, function(err) {
                    console.log(err)
                });

        };
        refreshIntervalId = $interval(function() {
            $rootScope.checkServicemenFlag = $localStorage.get('userMode');
            $scope.isLoggedIn();
            if ($scope.checkServicemen == "true") {
                $scope.sendLatLongFromServiceman();
            } else {

            }
        }, 120000);


        $scope.logout = function() {
            var obj = {
                "user": $localStorage.get('email')
            }
            //http://brings.co.in/Cluster/rest/Map
            var url = BRING_API_URL.apiEndpoint + "register/logout";
            apiFactory.postData(obj, url).then(function(success) {
                $rootScope.playerIdObject = $localStorage.get('playerId');
                $window.localStorage.clear();
                $scope.loggedIn = false;
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                location.reload();

                $localStorage.set('playerId', $rootScope.playerIdObject);

                $localStorage.set('cred', '');
                $localStorage.set('email', '');
                $localStorage.set('rootGlobals', '');
                $localStorage.set('rootGlobals1', '');
                $localStorage.set('userMode', '');
                $localStorage.set('userType', '');
                $localStorage.set('name', '');
                $localStorage.set('phone', '');
                $localStorage.set('storedUserLat', '');
                $localStorage.set('storedUserLon', '');
            });



        }
        $rootScope.checkServicemenFlag = $localStorage.get('userMode');



    })

    .controller('addressController', function($scope, $ionicModal, $timeout, $ionicNavBarDelegate, $localStorage, $rootScope, $ionicHistory, $window, latLongService, $http, $ionicHistory, BRING_API_URL, $cordovaGeolocation, $location, $interval, $cordovaSocialSharing, $ionicLoading, $ionicPlatform, latLongService, apiFactory) {
        $scope.addAddres = function(add) {
            if ($localStorage.get('email') != null) {

            } else {
                alert("Please login to continue");
                return;
            }

            if (document.getElementById('location1') != null) {
                var addressName = document.getElementById('location1').value;
            } else {

            }
            if (add.addrEmail == null || add.addrEmail == undefined || add.addrEmail == '') {

            }
            if (document.getElementById('location1') != null) {
                var addressName = document.getElementById('location1').value;
            }
            var dataObj = {
                email: $localStorage.get('email'),
                address_description: add.addrDesc,
                lat: "" + latLongService.toLat + "",
                lng: "" + latLongService.toLon + "",
                address: addressName,
                street: add.addrStreet,
                pincode: add.addrPinCode,
                flatNo: add.addrFlatNo,
                city: add.addrCityName,
                state: add.addrStateName
            }
            var url = BRING_API_URL.apiEndpoint + "SaveAddress/user";
            apiFactory.postData(dataObj, url).then(function(success) {
                alert("Address saved successfully");
                $location.path("/app/bring");
            });

        }

    })