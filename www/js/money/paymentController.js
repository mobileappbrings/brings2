angular.module('starter')
    .controller('paymentController', ["$scope", "$state", "$ionicConfig", "$http", "$ionicPopup", "$localStorage", "$location", "$controller", "$cordovaGeolocation", "$rootScope", "BRING_API_URL", "$timeout", function($scope, $state, $ionicConfig, $http, $ionicPopup, $localStorage, $location, $controller, $cordovaGeolocation, $rootScope, BRING_API_URL, $timeout, apiFactory) {


        console.log(localStorage.getItem("payuresponse"));


        // localStorage.setItem("payuresponse", {});

        $scope.getPayments = function() {



            if (localStorage.getItem("payuresponse") != null) {


                var payuResponseVar = localStorage.getItem("payuresponse");

                $localStorage.set("payuTxnId", getValue(payuResponseVar, 'mihpayid'));
                $localStorage.set("payuTxnStatus", getValue(payuResponseVar, 'status'));
                $localStorage.set("payuTxnModel", getValue(payuResponseVar, 'mode'));
                $localStorage.set("payuTxnBringTxnId", getValue(payuResponseVar, 'txnid'));
                $localStorage.set("payuTxnAmount", getValue(payuResponseVar, 'amount'));
                $localStorage.set("payuTxnFname", getValue(payuResponseVar, 'firstname'));


                $scope.payuTxnId = $localStorage.get("payuTxnId");
                $scope.payuTxnStatus = $localStorage.get("payuTxnStatus");
                $scope.payuTxnModel = $localStorage.get("payuTxnModel");
                $scope.payuTxnBringTxnId = $localStorage.get("payuTxnBringTxnId");
                $scope.payuTxnAmount = $localStorage.get("payuTxnAmount");
                $scope.payuTxnFname = $localStorage.get("payuTxnFname");


                var confirmPopup = $ionicPopup.confirm({
                    title: 'Success',
                    template: 'Successfully completed the payment...!!!'
                });

                confirmPopup.then(function(res) {
                    if (res) {
                        localStorage.setItem('payuresponse', "null");


                        if (localStorage.getItem('comingFrom') != undefined) {
                            var objToSend = {
                                "Email": localStorage.getItem("email"),
                                "txid": $scope.payuTxnId,
                                "amount": $scope.payuTxnAmount

                            };
                            var url = BRING_API_URL.apiEndpoint + "wallet/addMoney";
                            apiFactory.postData(objToSend, url).then(function(success) {

                            });

                            localStorage.setItem('comingFrom', undefined);
                        } else {
                            var objToSend = {
                                "WORK_ORDER_NUMBER": localStorage.getItem("WORK_ORDER_NUMBER"),
                                "PAY_TR_ID": $scope.payuTxnId

                            };
                            var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/paymentconf";
                            apiFactory.postData(objToSend, url).then(function(success) {

                            });

                        }




                    } else {
                        localStorage.setItem('payuresponse', "null");

                        var objToSend = {
                            "WORK_ORDER_NUMBER": localStorage.getItem("WORK_ORDER_NUMBER"),
                            "PAY_TR_ID": $scope.payuTxnId

                        };
                        var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/paymentconf";
                        apiFactory.postData(objToSend, url).then(function(success) {

                        });


                    }
                });
            }
        }

    }])
    .controller('addMoneyController', ["$scope", "$state", "$ionicConfig", "$http", "$ionicPopup", "$localStorage", "$location", "$controller", "$cordovaGeolocation", "$rootScope", "BRING_API_URL", "$timeout", function($scope, $state, $ionicConfig, $http, $ionicPopup, $localStorage, $location, $controller, $cordovaGeolocation, $rootScope, BRING_API_URL, $timeout, apiFactory) {

        $scope.addMoneyToBring = function(amt) {

            var obj = {
                "EMAIL": $localStorage.get('email')
            };
            var url = BRING_API_URL.apiEndpoint + "register/getUserProfile";
            apiFactory.postData(obj, url).then(function(success) {
                $localStorage.set('phone', res.CONTACT);
            });


            if (amt == undefined) {
                alert("Please enter money to add");
                return;
            }
            var timestamp = new Date().valueOf();
            $localStorage.set('paymentfailed', 0);
            localStorage.setItem('comingFrom', 'addMoneyPage');

            localStorage.setItem("bkeyVal", "rjQUPktU");
            localStorage.setItem("btxnid", timestamp);
            localStorage.setItem("bamount", amt);
            localStorage.setItem("bproductinfo", "GVN Softech");
            localStorage.setItem("bfirstname", $localStorage.get('name'));
            localStorage.setItem("bemail", $localStorage.get('email'));
            localStorage.setItem("bphone", $localStorage.get('phone'));
            localStorage.setItem("bsurl", "https://payu.herokuapp.com/success");
            localStorage.setItem("bfurl", "https://payu.herokuapp.com/failure");

            localStorage.setItem('payuresponse', "null");

            //rjQUPktU|d9b1b2dd1233dd4a4fedd3dd5d6d123979ba686003121|100|GVN Softech|tester123|test213@mailinator.com|||||||||||e5iIg1jwi8
            var shaValue = sha512(localStorage.getItem("bkeyVal") + "|" + localStorage.getItem("btxnid") + "|" + localStorage.getItem("bamount") + "|" + localStorage.getItem("bproductinfo") + "|" + localStorage.getItem("bfirstname") + "|" + localStorage.getItem("bemail") + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "e5iIg1jwi8");

            localStorage.setItem("bhash", shaValue);



            //$scope.onDeviceReadyTest1();
            $timeout(function() {
                onDeviceReadyTest();
            }, 1000);


        }

        $scope.checkOrderStatusPayu = function() {
            console.log("payment checking")
            var paymentfailed = localStorage.getItem('paymentfailed');

            if (paymentfailed == '500') {
                var alertPopup = $ionicPopup.alert({
                    title: 'Message',
                    template: '<center>Payment Failed</center>',
                    cssClass: 'popcss',
                    scope: $scope
                });
                $localStorage.set('paymentfailed', 0);
                $location.path("/app/addmoney");
            }
            var payuresponse = localStorage.getItem('payuresponse');

            if (payuresponse == null || payuresponse == 'null') {

            } else if (JSON.stringify(payuresponse) == '{}') {

            } else if (JSON.stringify(payuresponse) == 'null') {

            } else if (JSON.stringify(payuresponse) == null) {

            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Message',
                    template: '<center>Payment details will be displayed here...</center>',
                    cssClass: 'popcss',
                    scope: $scope
                });

                $location.path("/app/MyPayment");
            }


        }
        $scope.getWalletBalance = function() {
            var objToSend = {
                "Email": $localStorage.get('email')
            };
            var url = BRING_API_URL.apiEndpoint + "wallet/user";
            apiFactory.postData(objToSend, url).then(function(success) {
                $scope.commissionEarned = success.COMMISSION_EARNED;
                $scope.referralCredit = success.REFFERAL_CREDIT;
                $scope.joiningBonus = success.INITIAL_CREDIT;
                $scope.referral_id = success.referral_id;
                $scope.walletBal = success.WALLET_BALANCE;
            });

        }

    }])