angular.module('starter')
.controller('walletController', ["$scope","$state","$ionicConfig","$http","$ionicPopup","$localStorage","$location","$controller","$cordovaGeolocation","$rootScope","BRING_API_URL","$timeout","apiFactory",function($scope,$state, $ionicConfig,$http,$ionicPopup,$localStorage,$location,$controller,$cordovaGeolocation,$rootScope,BRING_API_URL,$timeout,apiFactory) {
	var checkLogin = $localStorage.get('email');
		if(checkLogin==undefined){
			alert("Please login to use the service");
			return;
		}
	$scope.getBankDetails =function(){
		 var objToSend={
			  "email" : $localStorage.get('email')
		  };
		   var url = BRING_API_URL.apiEndpoint + "uploadFileSystem/getbankdetails";
            apiFactory.postData(objToSend, url).then(function(success) {
				console.log(success)
			 $scope.bankDetailsObj =success;
			});
		  
	}
	$scope.addBankDetails =function(obj){
		var checkLogin = $localStorage.get('email');
		if(checkLogin==undefined){
			alert("Please login to use the service");
			return;
		}
		 if(obj.ACCOUNT_NAME==undefined){
			 alert("Please enter payee name");
			 return;
		 }
		 if(obj.ACCOUNT_NUMBER==undefined){
			 alert("Please enter account number");
			 return;
		 }
		 if(obj.BANK_NAME==undefined){
			 alert("Please enter bank name");
			return;
		 }
		 if(obj.IFSC==undefined){
			 alert("Please enter ifsc code");
			return;
		 }
		 if(obj.BRANCH==undefined){
			 alert("Please enter branch name");
			return;
		 }
			 var objToSend={
			  "Email" : $localStorage.get('email'),
			  "ACC_NAME" : obj.ACCOUNT_NAME,
			  "ACC_NUM" :obj.ACCOUNT_NUMBER,
			  "BANK_NAME" : obj.BANK_NAME,
			  "IFSC_CODE" : obj.IFSC,
			  "BRANCH" : obj.BRANCH
		  };
		    var url = BRING_API_URL.apiEndpoint + "uploadFileSystem/bankdetails";
            apiFactory.postData(objToSend, url).then(function(success) {
				 var alertPopup = $ionicPopup.alert ({
     title: 'Message',
     template: '<center>Successfully Updated Details...</center>',
	 cssClass : 'popcss',
	 scope : $scope
   });
			});
	 
		 
		 
		
	}
	$scope.getWalletBalance=function(){
		  var objToSend={
			  "Email" : $localStorage.get('email')
		  };
		   var url = BRING_API_URL.apiEndpoint + "uploadFileSystem/bankdetails";
            apiFactory.postData(objToSend, url).then(function(success) {
				$scope.commissionEarned = success.COMMISSION_EARNED; 
			 $scope.referralCredit = success.REFFERAL_CREDIT; 
			 $scope.joiningBonus = success.INITIAL_CREDIT; 
			 $scope.referral_id = success.referral_id; 
			 $scope.walletBal = success.WALLET_BALANCE; 
			  
			 $scope.getBankDetails();
			});
		 
	}
	

}])