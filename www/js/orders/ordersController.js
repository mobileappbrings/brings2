angular.module('starter')

    .controller("currOrderctrl", function($scope, $http, $localStorage, $ionicPopup, $location, $rootScope, userService, $compile, $timeout, tempService, $filter, $ionicLoading) {

        $scope.$on('$ionicView.enter', function() {
            if ($location.path() == '/app/currorderlist') {
                $scope.getCurrOrders();

            }
        });
        var todayDate = new Date();

        //ng-if="data.ORDER_DATE.split(' ')[0]==parsedDateToday"

        $scope.parsedDateToday = $filter('date')(new Date(todayDate), 'yyyy-MM-dd');



        //this is current order controller

        $scope.getCurrOrders = function() {
            if ($localStorage.get('userMode') == 'READYTODELIVER') {
                var obj = {
                    "email": $localStorage.get('email')
                };
                var url = BRING_API_URL.apiEndpoint + "OrderbyBuyer/getCurrentOrderService";
                apiFactory.postData(obj, url).then(function(succ) {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.getArrayOfOrders = succ;

                    for (var j = 0; j < $scope.getArrayOfOrders.length; j++) {


                        if ($scope.getArrayOfOrders[j].ORDER_DATE != null && $scope.getArrayOfOrders[j].ORDER_DATE != undefined) {
                            if ($scope.getArrayOfOrders[j].ORDER_DATE.split(' ')[0] == $scope.parsedDateToday) {
                                $scope.getArrayOfOrders[j].color = true;

                                $scope.getArrayOfOrders[j].ORDER_DATE = $filter('date')(new Date($scope.getArrayOfOrders[j].ORDER_DATE), 'dd-MM-yyyy');

                            } else {
                                $scope.getArrayOfOrders[j].color = false;
                                $scope.getArrayOfOrders[j].ORDER_DATE = $filter('date')(new Date($scope.getArrayOfOrders[j].ORDER_DATE), 'dd-MM-yyyy');
                            }
                        }


                        if ($scope.getArrayOfOrders[j].ITEMS) {
                            var ITEMS = $scope.getArrayOfOrders[j].ITEMS;
                            var ArrArr = [];
                            ITEMS = ITEMS.split("$");
                            for (var i = 0; i < ITEMS.length; i++) {
                                var objs = ITEMS[i].split("#");
                                console.log(objs);
                                ArrArr.push(objs);
                            }
                            console.log(ArrArr);
                            $scope.getArrayOfOrders[j].ITEMS = ArrArr;
                        }
                    }




                    console.log($scope.getArrayOfOrders)

                    $scope.$broadcast('scroll.refreshComplete');
                });

            } else {
                //{'background-color': 'yellow'}

                $http.put("http://brings.co.in/Cluster/rest/OrderbyBuyer/" + $localStorage.get('email')).success(function(succ) {

                    $scope.getArrayOfOrders = succ;
                    for (var i = 0; i < succ.length; i++) {
                        if (succ[i].ORDER_DATE != null && succ[i].ORDER_DATE != undefined) {
                            if (succ[i].ORDER_DATE.split(' ')[0] == $scope.parsedDateToday) {
                                $scope.getArrayOfOrders[i].color = true;
                                $scope.getArrayOfOrders[i].ORDER_DATE = $filter('date')(new Date($scope.getArrayOfOrders[i].ORDER_DATE), 'dd-MM-yyyy');

                            } else {
                                $scope.getArrayOfOrders[i].color = false;
                                $scope.getArrayOfOrders[i].ORDER_DATE = $filter('date')(new Date($scope.getArrayOfOrders[i].ORDER_DATE), 'dd-MM-yyyy');
                            }
                        }
                        if (succ[i].SERVICE_PERSON_CONFIRMATION == "YES") {

                            $scope.payOn = true;
                        }
                    }

                    if ($scope.payOn) {

                        $scope.$broadcast('scroll.refreshComplete');
                    }
                    $timeout(function() {

                    }, 1000);
                }).error(function(err) {

                });
            }

        }


        $scope.assignMeFun = function(data) {


            tempService.woid = data.WORK_ORDER_NUMBER;
            var obj = {
                "COMMENTS": "Approved",
                "WORK_ORDER_NUMBER": data.WORK_ORDER_NUMBER
            }
            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/confirmationbyserviceman", obj).success(function(succ) {
                console.log(succ)
                //$scope.getArrayOfOrders=succ;
                if (succ.success == 215) {


                    $timeout(function() {

                        alert("Please wait,you will get notification when buyer makes the payment");
                        $location.path('/app/dashboard');


                    }, 5000);
                }

            }).error(function(err) {

            });
        }

        $scope.cancelOrderByBuyerBeforePay = function(data) {
            var obj = {
                "WORK_ORDER_NUMBER": data.WORK_ORDER_NUMBER,
                "Email": $localStorage.get('email')
            }
            $http.post("http://brings.co.in/Cluster/rest/cancel/bybuyer", obj).success(function(succ) {

                if (succ.response == '-1') {
                    alert("Payment done so, order cannot be cancelled.");
                }
                if (succ.response == '1') {
                    alert("Order cancelled");
                }
                if (succ.response == '-2') {
                    alert("Server issue")
                }

            }).error(function(err) {

            });
        }
        $scope.cancelOrderByServiceman = function(data) {
            var obj = {
                "WORK_ORDER_NUMBER": data.WORK_ORDER_NUMBER,
                "Email": $localStorage.get('email')
            }
            $http.post("http://brings.co.in/Cluster/rest/cancel/bybuyer", obj).success(function(succ) {
                if (succ.response == '-1') {
                    alert("Payment done so, order cannot be cancelled.");
                }
                if (succ.response == '1') {
                    alert("Order cancelled");
                }
                if (succ.response == '-2') {
                    alert("Server issue")
                }

            }).error(function(err) {

            });
        }
        $scope.confirmationRateByServiceman = function(price, obj1, orderNo) {


            var strvar;

            strvar = obj1[0] + '!' + obj1[1] + '#' + price + '%';

            console.log(strvar)
            var sendObj = {
                "COMMENTS": "YES",
                "WORK_ORDER_NUMBER": orderNo,
                "items": strvar
            }
            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/confirmationbyservicemanforbuy", sendObj).success(function(succ) {
                console.log(succ)

                var popUp = $ionicPopup.alert({
                    title: 'Success' + JSON.stringify(succ),
                    template: ' ',
                    cssClass: 'popcss'
                });
                $timeout(function() {
                    $location.path("/app/currorderlist");
                }, 2000);
            }).error(function(err) {

            });
        }

        $scope.getOrderDetails = function(data) {

            $rootScope.detailsObj = "";
            //
            var sendObj = {
                "userId": $localStorage.get('email'),
                "WOD": data.WORK_ORDER_NUMBER,
                'role': $localStorage.get('userType')
            }
            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/getOrderById", sendObj).success(function(succ) {
                console.log(succ)
                if (succ.length > 0) {
                    $localStorage.set("WORK_ORDER_NUMBER", data.WORK_ORDER_NUMBER);


                    $rootScope.detailsObj = succ[0];

                    userService.userObj = succ[0];

                    if ($rootScope.detailsObj.ORDER_TYPE != 'BringGoods') {
                        var ITEMS = $rootScope.detailsObj.ITEMS;
                        var ArrArr = [];
                        var ArrArr2 = [];
                        ITEMS = ITEMS.split("$");
                        for (var i = 0; i < ITEMS.length; i++) {
                            var objs = ITEMS[i].split("#");
                            console.log(objs);
                            ArrArr.push(objs);
                        }
                        console.log(ArrArr);

                        $rootScope.detailsObj.ITEMS = ArrArr;
                        console.log($rootScope.detailsObj)
                    }
                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Message',
                        template: '<center>No Data Found...</center>',
                        cssClass: 'popcss',
                        scope: $scope
                    });
                }




            }).error(function(err) {

            });

        }
        $rootScope.openDialer = function(num) {

            if (num != null) {
                window.plugins.CallNumber.callNumber(function() {

                }, function() {

                }, num, true);

            }
        }
        $rootScope.getOrderDetailsFromPush = function(data) {

            $rootScope.detailsObj = "";
            //
            var sendObj = {
                "userId": $localStorage.get('email'),
                "WOD": data,
                'role': $localStorage.get('userType')
            }
            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/getOrderById", sendObj).success(function(succ) {
                console.log(succ)
                if (succ.length > 0) {
                    $localStorage.set("WORK_ORDER_NUMBER", data.WORK_ORDER_NUMBER);


                    $rootScope.detailsObj = succ[0];

                    userService.userObj = succ[0];

                    if ($rootScope.detailsObj.ORDER_TYPE != 'BringGoods') {
                        var ITEMS = $rootScope.detailsObj.ITEMS;
                        var ArrArr = [];
                        var ArrArr2 = [];
                        ITEMS = ITEMS.split("$");
                        for (var i = 0; i < ITEMS.length; i++) {
                            var objs = ITEMS[i].split("#");
                            console.log(objs);
                            ArrArr.push(objs);
                        }
                        console.log(ArrArr);

                        $rootScope.detailsObj.ITEMS = ArrArr;
                        console.log($rootScope.detailsObj)
                    }
                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Message',
                        template: '<center>No Data Found...</center>',
                        cssClass: 'popcss',
                        scope: $scope
                    });
                }




            }).error(function(err) {

            });

        }

        $scope.iniatedUserPay = function() {

            $localStorage.set('paymentfailed', 0);
            localStorage.setItem("bkeyVal", "rjQUPktU");
            localStorage.setItem("btxnid", userService.userObj.TRANSACTION_ID);
            localStorage.setItem("bamount", userService.userObj.totalbill);
            localStorage.setItem("bproductinfo", "GVN Softech");
            localStorage.setItem("bfirstname", $localStorage.get('name'));
            localStorage.setItem("bemail", $localStorage.get('email'));
            localStorage.setItem("bphone", $localStorage.get('phone'));
            localStorage.setItem("bsurl", "https://payu.herokuapp.com/success");
            localStorage.setItem("bfurl", "https://payu.herokuapp.com/failure");

            localStorage.setItem('payuresponse', "null");

            //rjQUPktU|d9b1b2dd1233dd4a4fedd3dd5d6d123979ba686003121|100|GVN Softech|tester123|test213@mailinator.com|||||||||||e5iIg1jwi8
            var shaValue = sha512(localStorage.getItem("bkeyVal") + "|" + localStorage.getItem("btxnid") + "|" + localStorage.getItem("bamount") + "|" + localStorage.getItem("bproductinfo") + "|" + localStorage.getItem("bfirstname") + "|" + localStorage.getItem("bemail") + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "|" + "e5iIg1jwi8");

            localStorage.setItem("bhash", shaValue);


            //$scope.onDeviceReadyTest1();
            $timeout(function() {
                onDeviceReadyTest();
            }, 1000);


        }
        $scope.checkOrderStatusPayu = function() {
            var paymentfailed = localStorage.getItem('paymentfailed');

            if (paymentfailed == '500') {
                var alertPopup = $ionicPopup.alert({
                    title: 'Message',
                    template: '<center>Payment Failed</center>',
                    cssClass: 'popcss',
                    scope: $scope
                });
                $localStorage.set('paymentfailed', 0);
                $location.path("/app/dashboard");
            }
            var payuresponse = localStorage.getItem('payuresponse');

            if (payuresponse == null || payuresponse == 'null') {

            } else if (JSON.stringify(payuresponse) == '{}') {

            } else if (JSON.stringify(payuresponse) == 'null') {

            } else if (JSON.stringify(payuresponse) == null) {

            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Message',
                    template: '<center>Payment details will be displayed here...</center>',
                    cssClass: 'popcss',
                    scope: $scope
                });
                $location.path("/app/MyPayment");
            }




        }
        $scope.someNumber = 15;
        $scope.apply = false;


        $scope.saveWorkId = function(data) {
            console.log(data)
            tempService.woid = data;
            $location.path("/app/liveTrack");

        }
        $scope.saveWorkId2 = function(data) {
            console.log(data)
            tempService.woid = data;


        }
        $scope.disableStart = false;
        $scope.start = function() {
            $scope.disableStart = true;
            var sendObj = {
                "WORK_ORDER_NUMBER": tempService.woid
            }
            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/startTrip", sendObj).success(function(succ) {
                if (succ.success == '252') {
                    alert("Success, You are on trip..!!!");

                } else {
                    alert("Please try later");
                }
            }).error(function(err) {

            });
        }
        $scope.stop = function() {
            var sendObj = {
                "WORK_ORDER_NUMBER": tempService.woid
            }
            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/stopTrip", sendObj).success(function(succ) {
                if (succ.success == '252') {
                    alert("Success, You stopped the trip...!!!");
                    alert("Please wait for buyer response...!!!");
                    var myPopup = $ionicPopup.show({
                        template: ' ',
                        scope: $scope,
                        buttons: [{
                                text: 'Cancel'
                            },
                            {
                                text: '<b>Delivered</b>',
                                type: 'button-positive',
                                onTap: function(e) {

                                    var obj = {
                                        "WORK_ORDER_NUMBER": tempService.woid
                                    }

                                    $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/delivered", obj).success(function(succ) {
                                        if (succ.success == '217') {
                                            alert("Thank you...!!!");
                                        } else {

                                        }
                                    }).error(function(err) {

                                    });


                                }
                            }
                        ]
                    });
                } else {
                    alert("Please try later");
                }
            }).error(function(err) {

            });
        }
    })
    


    .controller("liveTrackController", function($interval, $scope, $http, $localStorage, $ionicPopup, $location, $rootScope, userService, $compile, $timeout, tempService) {

        $scope.ratingArr = [{
            value: 1,
            icon: 'ion-ios-star-outline',
            question: 1
        }, {
            value: 2,
            icon: 'ion-ios-star-outline',
            question: 2
        }, {
            value: 3,
            icon: 'ion-ios-star-outline',
            question: 3
        }, {
            value: 4,
            icon: 'ion-ios-star-outline',
            question: 1
        }, {
            value: 5,
            icon: 'ion-ios-star-outline',
            question: 'question 5'
        }];
        $scope.serviceManReview = function(val) {

            var obj = {
                "WORK_ORDER_NUMBER": tempService.woid,
                "stars": "" + val + ""
            }
            if ($localStorage.get('userType') != 'buyer') {
                $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/servicemanReview", obj).success(function(succ) {
                    if (succ.success == '252') {
                        alert("Thank you...!!!");
                        $location.path('/app/dashboard');
                    } else {

                    }
                }).error(function(err) {

                });
            } else {
                $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/buyerReview", obj).success(function(succ) {
                    if (succ.success == '252') {
                        alert("Thank you...!!!");
                        $location.path('/app/dashboard');
                    } else {

                    }
                }).error(function(err) {

                });
            }


        }
        $scope.setRating = function(question, val) {
            var rtgs = $scope.ratingArr;
            for (var i = 0; i < rtgs.length; i++) {
                if (i < val) {
                    rtgs[i].icon = 'ion-ios-star';
                } else {
                    rtgs[i].icon = 'ion-ios-star-outline';
                }
            };

            $scope.serviceManReview(question);
        }

        $scope.UserRatingFlag = false;
        $scope.roleOfUser = $localStorage.get('userType');
        $scope.received = function() {
            var obj = {
                "WORK_ORDER_NUMBER": tempService.woid
            }

            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/received", obj).success(function(succ) {
                if (succ.success == '217') {
                    alert("Thank you...!!!");
                    alert("Please give rating for serviceman");

                    $scope.UserRatingFlag = true;
                } else {

                }
            }).error(function(err) {

            });
        }

        $scope.servicemanRatingFlag = false;
        $scope.delivered = function() {
            var obj = {
                "WORK_ORDER_NUMBER": tempService.woid
            }

            $http.post("http://brings.co.in/Cluster/rest/OrderbyBuyer/delivered", obj).success(function(succ) {
                if (succ.success == '217') {
                    alert("Thank you...!!!");
                    alert("Please give rating for user");
                    $scope.servicemanRatingFlag = true;
                } else {

                }
            }).error(function(err) {

            });
        }


        var latLongForDist1;
        var latLongForDist2;
        var latLongForDist3;
        var latLongForDist4;


        var finalDistance;
        var tempParts;
        var tempParts2;
        var tempParts3;
        var tempParts4;
        var tempParts5;

        var finalDistanceArray;
        var thresholdValue;
        $localStorage.set('thresholdValue', '');

        var origin1 = {
            lat: latLongForDist1,
            lng: latLongForDist2
        };
        var destinationA = {
            lat: latLongForDist3,
            lng: latLongForDist4
        };



        $scope.getTrackDetails = function() {
            var obj = {
                "work_order_number": tempService.woid
            }

            $http.post("http://brings.co.in/Cluster/rest/Map/currentLocation", obj).success(function(result) {
                if (result.RES == 'NO') {
                    alert("Serviceman not assigned..")
                    window.history.back();
                }
                $scope.resultData = result;

                getLatitudeLongitude(showResult, result.TO_ADDRESS);

                $timeout(function() {
                    latLongForDist1 = result.LAT;
                    latLongForDist2 = result.LNG;

                    console.log("latLongForDist1", result.LAT);
                    console.log("latLongForDist2", result.LNG);

                    origin1.lat = parseFloat(latLongForDist1);
                    origin1.lng = parseFloat(latLongForDist2);

                    getCity({
                        latitude: origin1.lat,
                        longitude: origin1.lng
                    }, function(city) {


                    });

                    init();
                    console.log(finalDistanceArray)
                    if (finalDistanceArray != null) {
                        finalDistanceArray = finalDistance.split(' ');
                    }

                    $scope.finalValue = tempParts;

                    tempParts = finalDistanceArray[0];

                    if ($localStorage.get('thresholdValue') == '' || $localStorage.get('thresholdValue') == {} || $localStorage.get('thresholdValue') == undefined) {
                        thresholdValue = tempParts / 4;
                        $localStorage.set('thresholdValue', thresholdValue);
                    }


                    tempParts2 = tempParts - tempParts / 2;

                    tempParts3 = tempParts - tempParts / 5;

                    tempParts4 = tempParts - tempParts / 7;

                    tempParts5 = tempParts - tempParts / 9;

                    if (tempParts < $scope.finalValue) {
                        $scope.showVar1 = 'active';
                        if (tempParts2 <= $localStorage.get('thresholdValue')) {
                            $scope.showVar1 = '';
                            $scope.showVar2 = 'active';
                        } else if (tempParts3 <= $localStorage.get('thresholdValue')) {
                            $scope.showVar2 = '';
                            $scope.showVar3 = 'active';
                        } else if (tempParts4 <= $localStorage.get('thresholdValue')) {
                            $scope.showVar3 = '';
                            $scope.showVar4 = 'active';
                        } else if (tempParts5 <= $localStorage.get('thresholdValue')) {
                            $scope.showVar1 = '';
                            $scope.showVar4 = 'active';
                            clearInterval(refreshIntervalId);

                        }


                    }




                }, 1000);

            }).error(function() {

            });
        }

        var i = 1;
        refreshIntervalId = $interval(function() {
            i++;

            $scope.getTrackDetails();
        }, 120000);


        function getLatitudeLongitude(callback, address) {

            address = address || 'Location undefined, please try again';

            geocoder = new google.maps.Geocoder();
            if (geocoder) {
                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        callback(results[0]);
                    }
                });
            }
        }

        function showResult(result) {


            latLongForDist3 = result.geometry.location.lat();
            latLongForDist4 = result.geometry.location.lng();

            destinationA.lat = latLongForDist3;
            destinationA.lng = latLongForDist4;




        }




        function init() {

            console.log(origin1)
            console.log(destinationA)

            var service = new google.maps.DistanceMatrixService;
            service.getDistanceMatrix({
                origins: [origin1],
                destinations: [destinationA],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function(response, status) {
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    alert('Error was: ' + status);
                    return;
                }
                var originList = response.originAddresses;
                var destinationList = response.destinationAddresses;

                var output2 = document.getElementById("myP");

                if (output2 != null) {
                    output2.innerHTML = "";

                    for (var i = 0; i < originList.length; i++) {
                        var results = response.rows[i].elements;
                        for (var j = 0; j < results.length; j++) {


                            output2.innerHTML += 'Total Distance ' + results[j].distance.text + ',without traffic may reach in ' + results[j].duration.text;
                            finalDistance = results[j].distance.text;
                            $scope.distBetween = output2.innerHTML;
                            console.log($scope.distBetween)

                        }
                    }
                    $timeout(function() {
                        output2.innerHTML = output2.innerHTML;
                        console.log($scope.distBetween)
                    }, 500);
                }
            });


        }

        function getCity(options, complete) {

            var geocoder = new google.maps.Geocoder(),
                request;

            if (options.latitude) {

                request = {
                    'latLng': new google.maps.LatLng(options.latitude, options.longitude)
                };

            } else {

                request = {
                    'address': options.address
                };

            };

            geocoder.geocode(request, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    $scope.servCurrLocation = results[0].formatted_address;


                } else {

                    console.log('error: ' + status);
                    complete();

                };

            });

        };

    })