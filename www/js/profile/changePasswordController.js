angular.module('starter')
.controller('changePasswordController', function($location,$scope,$state, $ionicConfig,$http,$ionicPopup,$localStorage,BRING_API_URL) {
	 $scope.changePass=function(obj){
	  var obj={
			 "email": $localStorage.get('email'),
			 "pwd": obj.userNewPassword
		};
		$http.post(BRING_API_URL.apiEndpoint+"register/changepass",obj).success(function(res){
			if(res.response==303){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure'+res.response,
     template: 'You password not changed...',
	 cssClass : 'popcssfamily',
	 scope : $scope
   });
			}else if(res.response==202){
				var alertPopup = $ionicPopup.alert ({
     title: 'Success'+res.response,
     template: 'You password has been changed...',
	 cssClass : 'popcssfamily',
	 scope : $scope
   });
   $location.path("/app/dashboard");
			}else if(res.response==403){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure'+res.response,
     template: 'There is a server issue, please try later...',
	 cssClass : 'popcssfamily',
	 scope : $scope
   });
			}
		}).error(function(err){
			
		});
		 
	 }
 })