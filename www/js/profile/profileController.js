angular.module('starter')

.controller('forgotPwdCtrl', ["$scope","$state","$ionicConfig","$http","$ionicPopup","$localStorage","$location","$controller","$cordovaGeolocation","$rootScope","$window","latLongService","BRING_API_URL","$cordovaCamera", "$cordovaFile", "$cordovaFileTransfer", "$cordovaDevice","$cordovaActionSheet","$cordovaInAppBrowser",function($scope,$state, $ionicConfig,$http,$ionicPopup,$localStorage,$location,$controller,$cordovaGeolocation,$rootScope,$window,latLongService,BRING_API_URL,$cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,$cordovaActionSheet,$cordovaInAppBrowser) {
	$scope.getPwd = function(formObj){
		 
		var obj={
			"EMAIL" : formObj.userMail
		}
		$http.post("http://brings.co.in/Cluster/rest/register/tempPassword",obj).success(function(succ){
			var alertPopup = $ionicPopup.alert ({
     title: 'Message'+succ.response,
     template: 'Password Sent to your please check once..',
	 cssClass : 'popcss',
	 scope : $scope
   });
		}).error(function(err){
			
		});
	}

}])

.controller('userController', ["$scope","$state","$ionicConfig","$http","$ionicPopup","$localStorage","$location","$controller","$cordovaGeolocation","$rootScope","$window","latLongService","BRING_API_URL","$cordovaCamera", "$cordovaFile", "$cordovaFileTransfer", "$cordovaDevice","$cordovaActionSheet","$cordovaInAppBrowser",function($scope,$state, $ionicConfig,$http,$ionicPopup,$localStorage,$location,$controller,$cordovaGeolocation,$rootScope,$window,latLongService,BRING_API_URL,$cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,$cordovaActionSheet,$cordovaInAppBrowser) {
 
 
 
 $scope.gPlace;
var googlePlaceAddress; 
  $scope.newPlace=function(chosenPlace){
    	console.log(chosenPlace);
    	googlePlaceAddress=chosenPlace;
    }
	$scope.doLogin=function(){
		var dest ;
		var lat;
		var lon;
	 if(googlePlaceAddress==undefined){
		 dest="user";
		 lat="";
		 lon="";
	 }else{
		 dest=googlePlaceAddress[0];
		 lat=googlePlaceAddress[1];
		 lon=googlePlaceAddress[2];
	 }		
		if($scope.passwordModel==undefined ){
			alert("please enter password");
			return;
		}
		if($scope.userName==undefined ){
			alert("please enter username");
			return;
		}
	 
	var obj={
			 "username": $scope.userName,
			 "password":$scope.passwordModel,
			 "buyerStatus": $scope.buyerMode,
			 "DESTINATION": latLongService.fromLoc,
			 "DEST_LAT":  ""+latLongService.fromLat+"",
			 "DEST_LNG":    ""+latLongService.fromLon+"",
			 "playerid" : $localStorage.get('playerId')
		};
		 console.log(obj)
		$http.post(BRING_API_URL.apiEndpoint+"register/login",obj).success(function(succ){
if(succ.response==301){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure'+succ.response,
     template: 'Invalid user',
	 cssClass : 'popcss',
	 scope : $scope
   });
			}else if(succ.response==302){
				var alertPopup = $ionicPopup.alert ({
     title: 'Wrong credentials',
     template: 'Wrong credentials',
	 cssClass : 'popcss',
	 scope : $scope
   });
			}else if(succ.response==401){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure',
     template: 'There is a server issue, please try later...',
	 cssClass : 'popcss',
	 scope : $scope
   });
			}else {
					
					SetCredentials($scope.userName);
					$scope.test= $scope.$parent.isLoggedIn();
					$localStorage.set('email', $scope.userName);
					console.log($scope.buyerMode)
					
					if($scope.buyerMode=="READYTODELIVER"){
						$localStorage.set('cred', true);						
						$localStorage.set('userMode',$scope.buyerMode);
						$localStorage.set('userType', "serviceman");
						$rootScope.checkServicemenFlag = $localStorage.get('userMode');
						location.reload();
						
   $scope.lat=null;
		$scope.longt=null
		   var posOptions = {timeout: 10000, enableHighAccuracy: false};

	$cordovaGeolocation
   .getCurrentPosition(posOptions)
	
   .then(function (position) {
      $scope.lat  = position.coords.latitude;
      $scope.longt = position.coords.longitude;
	  
     console.log($scope.lat);
	 var obj=  {"lat": ""+$scope.lat+"","lon": ""+$scope.longt+"","userid":$localStorage.get("email")}
		  
		  $http.post(BRING_API_URL.apiEndpoint+"Map",obj).success(function(succ){
			  console.log(succ)
		  }).error(function(err){
			  
		  });
		  
   }, function(err) {
      console.log(err)
   });
   
					}
					if(succ.role=='buyer'){
						$localStorage.set('cred', false);
						$localStorage.set('userMode','buy');
						$localStorage.set('userType', "buyer");
						
						$scope.getUser(); 
					}
								
				  var alertPopup = $ionicPopup.alert ({
     title: 'Success',
     template: 'You have successfully loggedin...',
	 cssClass : 'popcss',
	 scope : $scope
   });
    
//   location.reload();
	$location.path("#/app/dashboard")
			}}).error(function(err){
			
		});
	
		 
	}
	
	$scope.destinationBox=false;
		
	$scope.doIfChecked=function(val){
		if(val==true)
		{
			$scope.buyerMode="READYTODELIVER";
			$scope.destinationBox=true;
		
		}else{
			$scope.buyerMode="buy";
		$scope.destinationBox=false;
			
		} 
		console.log($scope.buyerMode)	
	}
	 
	function SetCredentials(obj) {
			$localStorage.set('email', obj);
			$localStorage.set('rootGlobals',true);
			
			 $window.localStorage.setItem('rootGlobals1', true);
    }
	 
     function ClearCredentials() {
			$localStorage.set('email', {});
			$localStorage.set('rootGlobals',false);
						 $window.localStorage.setItem('rootGlobals1', false);

     }
	 $scope.getUser=function(){
		  var obj={
			  "EMAIL" : $localStorage.get('email')
		  }
		 $http.post(BRING_API_URL.apiEndpoint+"register/getUserProfile",obj).success(function(res){
			 
			 $scope.Profileusername = res.USERNAME;
			 $scope.Profilecontact = res.CONTACT;
			 $scope.Profileaddress =  res.ADDRESS;
			 $scope.ProfileAge =  res.AGE;
		  
			 $localStorage.set('name', res.USERNAME);
			 $localStorage.set('phone', res.CONTACT);
			 
			  
			 
		 }).error(function(err){
			 
		 });
		 
		 var posOptions = {timeout: 10000, enableHighAccuracy: false};

			$cordovaGeolocation
		   .getCurrentPosition(posOptions)
			
		   .then(function (position) {
			   $localStorage.set('storedUserLat',position.coords.latitude); 
			   $localStorage.set('storedUserLon',position.coords.longitude);
			      
		   }, function(err) {
			  console.log(err)
		   });
	 }
	$scope.Profileemail =$localStorage.get('email');
	
	$scope.watchUserName =function(data){
		$scope.Profileusername = data;
	}
	 $scope.watchMob =function(data){
		$scope.Profilecontact = data;
	}
	$scope.watchAddr =function(data){
		$scope.Profileaddress = data;
	}
	$scope.watchAge =function(data){
		$scope.ProfileAge = data;
	}
	 $scope.updateUser=function(){
		  
			var obj={
				"username":$scope.Profileusername,
				"phno":$scope.Profilecontact,
				"address":$scope.Profileaddress,
				"email":$localStorage.get('email'),
				"age" : $scope.ProfileAge
				}
		 $http.post(BRING_API_URL.apiEndpoint+"register/updateProfile",obj).success(function(res){
			  
			  alert("profile updated successfully");
		 }).error(function(err){
			 
		 });
	 }
	  $scope.getImage=function(){
			var obj={
				 
				"Email":$localStorage.get('email') 
				}
		 $http.post(BRING_API_URL.apiEndpoint+"general/getImage",obj).success(function(res){
			  
			   console.log(res)
			   $scope.userImage= res.IMG_URL;
		 }).error(function(err){
			 
		 });
	 }
	  $scope.getImage();
	$scope.openSocialService=function(data){
		var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
   };
    
   if(data == 'facebook'){
	   $cordovaInAppBrowser.open('http://facebook.com', '_blank', options)
		
      .then(function(event) {
         // success
      })
		
      .catch(function(event) {
         // error
      }); 
   }else{
	    $cordovaInAppBrowser.open('http://gmail.com', '_blank', options)
		
      .then(function(event) {
         // success
      })
		
      .catch(function(event) {
         // error
      });   
   }
   
		 
	}
  
  
  
  
 $scope.fileupload2213  = function(){
		  
	 var fileInputAadhar = document.getElementById('uploadedFileAadhar');
	var fileInputDrivingLic = document.getElementById('uploadedFileDrivingLic');
	var fileInputVehpic = document.getElementById('uploadedFileVehiclePhoto');
	var fileInputRC = document.getElementById('uploadedFileVehicleRc');
	var fileInputPan = document.getElementById('uploadedFilePanCard');
	var fileInputhto = document.getElementById('uploadedFilePhto');
  
  
		var file1 = fileInputAadhar.files[0];
		var file2 = fileInputDrivingLic.files[0];
		var file3 = fileInputVehpic.files[0];
		var file4 = fileInputRC.files[0];
		var file5 = fileInputPan.files[0];
		var file6 = fileInputhto.files[0];
		
		var formData = new FormData();
		formData.append('aadharcard', file1);
		formData.append('drivinglicense', file2);
		formData.append('vehiclephoto', file3);
		formData.append('vehivclerc', file4);
		formData.append('pancard', file5);
		formData.append('passportphoto', file6);
		formData.append('UserId', $localStorage.get('email'));
 
		 
		$http.post(BRING_API_URL.apiEndpoint+"uploadFileSystem/"+$localStorage.get('email'), formData, {
			transformRequest: angular.identity,
			headers: {
				'Content-Type': undefined
			}
		}).success(function(succ){
			if(succ.response=="234"){
			 var alertPopup = $ionicPopup.alert ({
				     title: 'Success',
				     template: 'Successfullly uploaded into server...',
					 cssClass : 'popcss',
					 scope : $scope
				   });
				   $location.path("/app/dashboard");
			}else{
				 var alertPopup = $ionicPopup.alert ({
				     title: 'Message',
				     template: 'Problem in uploading into server please try again',
					 cssClass : 'popcss',
					 scope : $scope
				   });
			}
		}).error(function(){
			
		});
 				  
					 
 }
  
  
  
}])