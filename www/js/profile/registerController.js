angular.module('starter')
.controller('registerController', ["$scope","$state","$ionicConfig","$http","$ionicPopup","$localStorage","$location","$controller","$cordovaGeolocation","$rootScope","BRING_API_URL","$timeout","$cordovaFileTransfer","$cordovaActionSheet","$cordovaCamera","$cordovaDevice","$cordovaFile",function($scope,$state, $ionicConfig,$http,$ionicPopup,$localStorage,$location,$controller,$cordovaGeolocation,$rootScope,BRING_API_URL,$timeout,$cordovaFileTransfer,$cordovaActionSheet,$cordovaCamera,$cordovaDevice,$cordovaFile) {
 
  
 $rootScope.loggedIn = $localStorage.get('rootGlobals');
 $scope.securityQuestions =[
  {
	  "id" : "1",
	  "question" : "What is 1"
	  
  },{
	  "id" : "2",
	  "question" : "What is 2"
	  
  }
  ];
  
  
	 $ionicConfig.backButton.text("")
	$scope.gologin=function(){
		$state.go('app.register');
	}
	$scope.submitlogin=function(){
		swal("Successfully logged In!", "", "success")
		$state.go('app.dashboard');
	}
	
	$scope.showValidationMob =function(data){
		$scope.invalidMob=false;
		if(data.length > 9){
		 
			if(data < 7000000000){
		 $scope.invalidMob=true;
			
		}
		}
		
	}
	$scope.showValidationAge =function(data){
		$scope.invalidAge=false;
		if(data < 01){
			 $scope.invalidAge=true;
		}
		
	}
	$scope.formObj={};
	$scope.checkBothPasswords =function(){
		
		 //invalidPwd
		  $scope.invalidPwd=false;
		  if($scope.formObj.UserPass !=  $scope.formObj.UserPassConf){
			  $scope.invalidPwd=true;
		  }
	}
	$scope.checkPassLen = function(){
		$scope.invalidPwdLen=false;
		if($scope.formObj.UserPass.length < 3){
			$scope.invalidPwdLen=true;
		}
	}
	
	$scope.doREg=function(formModel){
		
	 if(formModel.referralId==null || formModel.referralId==undefined){
		 formModel.referralId = "NA";
	 }
	
		
		var obj={
			"name": formModel.UserName,
			"age": formModel.UserAge,
			"contact": formModel.UserContact,
			"email": formModel.UserEmail,
			"gender": formModel.genderForReg,
			"password": formModel.UserPass,
			"address": formModel.address,
			"category":"DUMMY",
			"latitude": "17.47",
			"longitude": "78.44",
			"secques1":  "What is your child hood name",
			"secques2": "What is your school name",
			"secans1": formModel.securityA,
			"secans2": formModel.securityB,
			"playerid" : $localStorage.get('playerId'),
			"referralid" : formModel.referralId
		}
		    
		$http.post(BRING_API_URL.apiEndpoint+"register/user",obj).success(function(succ){
			console.log(succ)
			if(succ.response==301){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure' ,
     template: 'User already exist, please signup with new email id...',
	 cssClass : 'popcs',
	 scope : $scope
   });
			}else if(succ.response==367){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure' ,
     template: 'Problem please check error code',
	 cssClass : 'popcs',
	 scope : $scope
   });
			}else if(succ.response==401){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure' ,
     template: 'There is a server issue, please try later...',
	 cssClass : 'popcss',
	 scope : $scope
   });
			}else {
				  var alertPopup = $ionicPopup.alert ({
     title: 'Success',
     template: 'You have successfully registered...',
	 cssClass : 'popcss',
	 scope : $scope
   });
  // location.reload();
$state.go('app.dashboard');	

$scope.formModel={};
			}
		}).error(function(err){
			
		});
		//http://brings.co.in/Cluster/rest/register/user
				
 	
	}
 
 
	
	$scope.doREgMinimalFields=function(formModel){
		if(formModel.UserName==undefined){
			alert("please enter your name");
			return;
			
		}
		if(formModel.UserContact==undefined){
			alert("please enter your phone no.");
			return;
			
		}
		if(formModel.UserEmail==undefined){
			alert("please enter your emailid");
			return;
			
		}
		if(formModel.UserPass==undefined){
			alert("please enter your password");
			return;
			
		}
		
	 
	 if(formModel.referralId==null || formModel.referralId==undefined){
		 formModel.referralId = "NA";
	 }
		 
		var obj={
			"name": formModel.UserName,
			"contact": formModel.UserContact,
			"email": formModel.UserEmail,
			"password": formModel.UserPass,
			"latitude": "17.47",
			"longitude": "78.44",
			"playerid" : $localStorage.get('playerId'),
			"referralid" : formModel.referralId
		}
		    
		$http.post(BRING_API_URL.apiEndpoint+"register/custom",obj).success(function(succ){
			console.log(succ)
			if(succ.response==301){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure',
     template: 'User already exist, please signup with new email id...',
	 cssClass : 'popcs',
	 scope : $scope
   });
			}else if(succ.response==367){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure',
     template: 'Problem please check error code',
	 cssClass : 'popcs',
	 scope : $scope
   });
			}else if(succ.response==401){
				var alertPopup = $ionicPopup.alert ({
     title: 'Failure'+succ.response,
     template: 'There is a server issue, please try later...',
	 cssClass : 'popcss',
	 scope : $scope
   });
			}else {
				  var alertPopup = $ionicPopup.alert ({
     title: 'Success',
     template: 'You have successfully registered...',
	 cssClass : 'popcss',
	 scope : $scope
   });
  // location.reload();
$state.go('app.dashboard');	

$scope.formModel={};
			}
		}).error(function(err){
			
		});
		//http://brings.co.in/Cluster/rest/register/user
				
 	
	}
  
}])