angular.module('starter').config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.backButton.previousTitleText(false);
        $stateProvider
			.state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'js/home/menu.html',
                cache: false,
                controller: 'AppCtrl'
            })
			.state('app.login', {
                url: '/login',
                resolve: {
                deps: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('bring');
                }
				},
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        cache: false,
                        controller: 'userController'
                    }
                }
            })
            .state('app.references', {
                url: '/references',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/references.html',
                        cache: false
                    }
                }
            })
            .state('app.addmoney', {
                url: '/addmoney',
                views: {
                    'menuContent': {
                        templateUrl: 'js/money/AddMoney.html',
                        cache: false
                    }
                }
            })
            .state('app.rating', {
                url: '/rating',
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/ratingScreen.html',
                        cache: false
                    }
                }
            })
            .state('app.addaddress', {
                url: '/addaddress',
                resolve: {
                deps: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('bring');
                }
				},
                views: {
                    'menuContent': {
                        templateUrl: 'js/home/addaddress.html',
                        cache: false,
                        controller: 'addressController'
                    }
                }
            })
            .state('app.forgotPwd', {
                url: '/forgotPwd',
                views: {
                    'menuContent': {
                        templateUrl: 'js/profile/forgotPwd.html',
                        cache: false,
                        controller: 'userController'
                    }
                }
            })
            .state('app.startstop', {
                url: '/startstop',
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/startstop.html',
                        cache: false,
                        controller: 'userController'
                    }
                }
            })
            .state('app.MyProfile', {
                url: '/MyProfile',
                resolve: {
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/profile/profileController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/profile/MyProfile.html',
                        cache: false
                    }
                }
            })
            .state('app.offers', {
                url: '/offers',
                views: {
                    'menuContent': {
                        templateUrl: 'js/offers/offers.html',
                        cache: false
                    }
                }
            })
            .state('app.liveTrack', {
                url: '/liveTrack',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/liveTrack.html',
                        cache: false
                    }
                }
            })
            .state('app.miniDetails', {
                url: '/miniDetails',
                resolve: {
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/statement/statementController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/statement/miniDetails.html',
                        controller: 'MyCtrl',
                        cache: false
                    }
                }
            })
            .state('app.notifc', {
                url: '/notifc',
                views: {
                    'menuContent': {
                        templateUrl: 'js/statement/notifc.html',
                        cache: false
                    }
                }
            })
            .state('app.MyWallet', {
                url: '/MyWallet',
				resolve :{
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/money/walletController.js");
				}]
				},
                views: {
                    'menuContent': {
                        templateUrl: 'js/money/MyWallet.html',
                        cache: false
                    }
                }
            }).state('app.about', {
                url: '/about',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/about.html',
                        cache: false
                    }
                }
            })
            .state('app.MyBank', {
                url: '/MyBank',
                views: {
                    'menuContent': {
                        templateUrl: 'js/money/MyBank.html',
                        cache: false
                    }
                }
            })
            .state('app.mini', {
                url: '/mini',
                views: {
                    'menuContent': {
                        templateUrl: 'js/statement/mini.html',
                        cache: false
                    }
                }
            })
            .state('app.register', {
                url: '/register',
				resolve : {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/profile/registerController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/register.html',
                        cache: false,
                        controller: 'registerController'
                    }
                }
            })
			.state('app.dashboard', {
                url: '/dashboard',
                views: {
                    'menuContent': {
                        templateUrl: 'js/home/dashboard.html',
                        cache: false,
                        controller: 'PlaylistsCtrl'
                    }
                }
            })
            .state('app.bringbuy', {
                url: '/bringbuy',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/buy/buyController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/buy/buy.html',
                        cache: false,
                        controller: 'buyController'
                    }
                }
            })
            .state('app.bring', {
                url: '/bring',
                resolve:  {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/bring/bringController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/bring/bring.html',
                        cache: false,
                        controller: 'bringController'
                    }
                }
            }).state('app.summary', {
                url: '/summary',
                resolve:  {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/bring/orderSummary.html',
                        cache: false
                    }
                }
            })
            .state('app.trackorder', {
                url: '/trackorder',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/trackorder.html',
                        cache: false,
                        controller: 'myOrdersCtrl'
                    }
                }
            })
            .state('app.changePass', {
                url: '/changePass',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/profile/changePasswordController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/profile/changePass.html',
                        cache: false,
                        controller: 'changePasswordController'
                    }
                }
            })
            .state('app.currentorders', {
                url: '/currentorders',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/currentorders.html',
                        cache: false

                    }
                }
            })
            .state('app.currdetails', {
                url: '/currdetails',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/currdetails.html',
                        cache: false

                    }
                }
            })
            .state('app.MyPayment', {
                url: '/MyPayment',
                resolve: {
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/money/ordersController.js","js/money/paymentController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/money/MyPayment.html',
                        cache: false

                    }
                }
            })
            .state('app.currorderlist', {
                url: '/currorderlist',
                resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/currorderlist.html',
                        controller: 'currOrderctrl',
                        cache: false

                    }
                }
            })
            .state('app.waitingscreen', {
                url: '/waitingscreen',
                views: {
                    'menuContent': {
                        templateUrl: 'js/bring/waitingscreen.html',
                        cache: false

                    }
                }
            })
            .state('app.payubizz', {
                url: '/payubizz',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/payuBiz.html',
                        cache: false

                    }
                }
            })
            .state('app.trackdetails', {
                url: '/trackdetails',
				resolve: {  
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				    return $ocLazyLoad.load("js/orders/ordersController.js");
				}]
			  },
                views: {
                    'menuContent': {
                        templateUrl: 'js/orders/trackdetails.html',
                        cache: false,
                        controller: 'PlaylistsCtrl'
                    }
                }
            });
        $urlRouterProvider.otherwise('/app/dashboard');
    })
.config(function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: true,
        events: true,
        modules: [{
            name: "bring",
            files: [
                "js/common/LocalStorage.js",
				"js/ApiFactory.js","js/home/homeController.js"
            ],
        }]
    });
});